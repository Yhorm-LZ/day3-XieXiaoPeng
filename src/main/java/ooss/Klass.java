package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private final Integer number;
    private Student leader;
    private final List<Person> personList = new ArrayList<>();

    public Klass(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Klass klass = (Klass) obj;
        return Objects.equals(this.number, klass.number);
    }

    public void assignLeader(Student leader) {
        if (!leader.isIn(this)) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = leader;
        notifyAllPerson();
    }

    public void notifyAllPerson() {
        for (Person person : personList) {
            person.responseToNotify(this, leader);
        }
    }

    public boolean isLeader(Student student) {
        return Objects.equals(this.leader,student);
    }

    public void attach(Person person) {
        if(!personList.contains(person)) {
            this.personList.add(person);
        }
    }
}
