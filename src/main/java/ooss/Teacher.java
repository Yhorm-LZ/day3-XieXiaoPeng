package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private final List<Klass> klassList = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduction = new StringBuilder();
        if (klassList.size() == 0) {
            return String.format("My name is %s. I am %d years old. I am a teacher.", name, age);
        }
        introduction.append(String.format("My name is %s. I am %d years old. I am a teacher. I teach Class ", name, age));
        for (int i = 0; i < klassList.size(); i++) {
            introduction.append(klassList.get(i).getNumber());
            if (i != klassList.size() - 1) {
                introduction.append(", ");
            }
        }
        introduction.append(".");
        return introduction.toString();
    }

    public void assignTo(Klass klass) {
        if (this.belongsTo(klass)) {
            return;
        }
        this.klassList.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return klassList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return this.klassList.contains(student.getKlass());
    }

    @Override
    public void responseToNotify(Klass klass, Student leader) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", this.name, klass.getNumber(), leader.name);
    }
}
